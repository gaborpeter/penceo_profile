(function ($) {
  Drupal.behaviors.penceo_ipe_templates = {
    attach: function (context, settings) {
      if (typeof(imagesLoaded) == 'function') {
        var $ipe_fia_container = $('.ipe-template-list').imagesLoaded(function () {
          if (!(typeof $ipe_fia_container == 'undefined') && typeof $ipe_fia_container.isotope == 'function') {
            $ipe_fia_container.isotope({
              itemSelector: '.template-item'
            });
          }
        });
      }
      else {
        setTimeout(function () {
          $('.ipe-template-list').isotope({
            itemSelector: '.template-item'
          });
        }, 1000);
      }
    }
  }
})(jQuery);
