<?php
/**
 * @file Helper function storage file.
 */

/**
 * Returns available profiles.
 *
 * @return array
 */
function penceo_ipe_templates_get_profiles() {
  return variable_get('penceo_ipe_templates_profiles', array());
}

/**
 * Checks if given provile exists.
 *
 * @param string $profile
 *  the machine name of the profile
 * @return bool true if exists
 *  false when not
 */
function penceo_ipe_templates_profile_exists($profile = '') {
  $profiles = penceo_ipe_templates_get_profiles();
  return isset($profiles[$profile]);
}

/**
 * Returns default pane types.
 *
 * @return array
 */
function penceo_ipe_templates_pane_type_defaults() {
  return array(
    'fieldable_panels_pane' => 'Fieldable panels pane',
  );
}

/**
 * Returns pane <=> profile combinations with all available settings.
 *
 * @return array
 */
function penceo_ipe_templates_get_pane_presets() {
  return variable_get('penceo_ipe_templates_pane_presets', array());
}