<?php
/**
 * @file
 * Admin form holder for penceo IPE templates.
 */


/**
 * Default form builder.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 *
 */
function penceo_ipe_templates_admin_form($form, &$form_state) {

  module_load_include('inc', 'penceo_ipe_templates', 'inc/penceo_ipe_templates.helpers');

  $form['pane_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pane types'),
    '#description' => t('Default pane list settings.'),
  );

  $form['pane_types']['penceo_ipe_templates_project_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Project prefix'),
    '#default_value' => variable_get('penceo_ipe_templates_project_prefix', ''),
    '#description' => t('The project name prefix of a custom panel pane (<i>For instance: <strong>penceo</strong>_my_custom_pane</i>.'),
    '#size' => 5,
  );

  $form['pane_types']['penceo_ipe_templates_pane_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Pane types'),
    '#options' => penceo_ipe_templates_pane_type_defaults(),
    '#default_value' => variable_get('penceo_ipe_templates_pane_types', penceo_ipe_templates_pane_type_defaults()),
    '#description' => t('The list of default pane types that will be enabled in the pane list.'),
  );

  return system_settings_form($form);
}

/**
 * Default form builder.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 *
 */
function penceo_ipe_templates_profile_form($form, &$form_state) {

  module_load_include('inc', 'penceo_ipe_templates', 'inc/penceo_ipe_templates.helpers');

  $form['profile'] = array(
    '#title' => t('Add new profile'),
    '#type' => 'fieldset',
    '#weight' => -10,
  );

  $form['profile_list'] = array(
    '#title' => t('Stored profiles'),
    '#description' => t('In the list above you can see the available profiles stored. <b>NOTE:</b> When a profile is unchecked upon save it will be completly removed!'),
    '#type' => 'fieldset',
    '#weight' => -9,
    '#tree' => TRUE,
  );

  $profiles = penceo_ipe_templates_get_profiles();
  if (empty($profiles)) {
    $form['profile_list']['no_result'] = array(
      '#markup' => theme('html_tag', array(
        'element' => array(
          '#tag' => 'div',
          '#attributes' => array('id' => 'available-profiles'),
          '#value' => t('There is no existing profile. Add a new one above!'),
        )
      )),
    );
  }
  else {
    foreach ($profiles as $machine_name => $name) {
      $form['profile_list'][$machine_name] = array(
        '#type' => 'checkbox',
        '#title' => t($name),
        // These are currently enabled at this point.
        '#default_value' => 1,
      );
    }
  }

  $form['profile']['profile_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile name'),
    '#description' => t('Type in the name of the profile.'),
    '#default_value' => '',
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['profile']['profile_machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Profile machine name'),
    '#description' => t('Type in the unique name of a profile.'),
    '#maxlength' => 30,
    '#machine_name' => array(
      'exists' => '_penceo_ipe_machine_name_exists',
    ),
  );

  $form['profile']['profile_create'] = array(
    '#type' => 'submit',
    '#value' => t('Add new profile'),
    '#submit' => array('_penceo_ipe_templates_profile_add_profile'),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
    '#submit' => array('_penceo_ipe_templates_profile_save_settings'),
    '#limit_validation_errors' => array(),
  );

  return $form;
}

/**
 * Callback function for the machine name check.
 * @param string $profile_name
 * @return bool
 */
function _penceo_ipe_machine_name_exists($profile_name) {
  return penceo_ipe_templates_profile_exists($profile_name);
}

/**
 * Add profile submit callback.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function _penceo_ipe_templates_profile_add_profile($form, &$form_state) {
  $profile = array(
    'name' => $form_state['values']['profile_name'],
    'machine_name' => $form_state['values']['profile_machine_name'],
  );
  $current_profiles = penceo_ipe_templates_get_profiles();
  $current_profiles[$profile['machine_name']] = $profile['name'];
  variable_set('penceo_ipe_templates_profiles', $current_profiles);
}

/**
 * Profile save settings callback.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function _penceo_ipe_templates_profile_save_settings($form, &$form_state) {
  if (!empty($form_state['input']['profile_list'])) {
    $profiles = penceo_ipe_templates_get_profiles();
    // Let's load our presets in case something needs to be deleted.
    $pane_presets = penceo_ipe_templates_get_pane_presets();
    // Indicates if we have to refresh already saved items.
    $refresh = FALSE;

    foreach (array_keys($form_state['input']['profile_list']) as $machine_name) {
      if (isset($profiles[$machine_name]) && !$form_state['input']['profile_list'][$machine_name]) {
        // Let's check if we need to remove some configuration as well.
        foreach ($pane_presets as $pane => $pane_settings) {
          if (isset($pane_presets['profiles'])
            && in_array($machine_name, $pane_presets['profiles'])
          ) {
            unset($pane_presets[$pane]['profiles'][$machine_name]);
            $refresh = TRUE;
          }
        }
        unset($profiles[$machine_name]);
      }
    }
    // Save profiles information.
    variable_set('penceo_ipe_templates_profiles', $profiles);
    // Resave pane presets in case of change.
    if ($refresh) {
      variable_set('penceo_ipe_templates_pane_presets', $pane_presets);
    }
  }
}

/**
 * Default form builder.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 *
 */
function penceo_ipe_templates_panes_form($form, &$form_state) {

  module_load_include('inc', 'penceo_ipe_templates', 'inc/penceo_ipe_templates.helpers');

  $project_prefix = variable_get('project_prefix');
  // Get available panes to be added to profiles.
  $panes = _penceo_ipe_templates_get_panes($project_prefix);

  $form_state['storage']['panes'] = $panes;

  // Get currently set profiles.
  $profiles = penceo_ipe_templates_get_profiles();
  // Get already saved pane <=> profile combinations
  $pane_presets = penceo_ipe_templates_get_pane_presets();

  $form['pane_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pane settings'),
    '#description' => t('Attach content panes that should be available when the specific profile is used.'),
    '#tree' => TRUE,
  );

  foreach ($panes as $type => $ct) {
    $form['pane_settings'][$type] = array(
      '#type' => 'fieldset',
      '#title' => t($ct['name']),
      '#description' => $ct['description'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );

    $form['pane_settings'][$type]['thumbnail_image'] = array(
      '#type' => 'managed_file',
      '#name' => 'thumbnail_image',
      '#title' => t('Thumbnail image'),
      '#description' => t('Attach a desired image to the selected pane. <i>This is visible in the IPE view.</i>'),
      '#upload_location' => 'public://penceo_ipe_templates/',
      '#default_value' => isset($pane_presets[$type]['thumbnail_image']) ? $pane_presets[$type]['thumbnail_image'] : '',
      '#upload_validators' => array(
        'file_validate_extensions' => array('jpg jpeg png'),
      ),
    );

    $form['pane_settings'][$type]['profiles'] = array(
      '#type' => 'fieldset',
      '#title' => t('Available profiles'),
      '#description' => t('Here you can see the available profiles that have been previously set at the <i>Profile creation</i> tab.'),
      '#tree' => TRUE,
    );

    foreach ($profiles as $machine_name => $name) {
      $form['pane_settings'][$type]['profiles'][$machine_name] = array(
        '#type' => 'checkbox',
        '#title' => t($name),
        '#default_value' => isset($pane_presets[$type]['profiles'][$machine_name]) ? TRUE : FALSE,
      );
    }
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
    '#submit' => array('_penceo_ipe_templates_pane_settings_save'),
  );

  return $form;
}

function _penceo_ipe_templates_pane_settings_save($form, &$form_state) {
  $panes = $form_state['values']['pane_settings'];

  $settings = array();
  foreach ($panes as $pane_name => $pane) {
    if (isset($pane['profiles'])) {
      foreach ($pane['profiles'] as $profile_name => $value) {
        if ($value) {
          // Merge default pane data with the selected ones.
          $settings[$pane_name] = $form_state['storage']['panes'][$pane_name];
          $settings[$pane_name]['profiles'][$profile_name] = TRUE;
        }
      }
    }

    // Let's check if we have an attached image for the pane.
    if ($pane['thumbnail_image']) {
      $file = file_load($pane['thumbnail_image']);
      if (!$file) {
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
      }
      $settings[$pane_name]['thumbnail_image'] = $file->fid;
    }
  }

  variable_set('penceo_ipe_templates_pane_presets', $settings);

}

/**
 * Queries all available panes for setup.
 *
 * @param string $project_prefix
 *  Project prefix helps to find the custom panes.
 * @param array $types
 *  an array of pane types
 *
 * @return array
 */
function _penceo_ipe_templates_get_panes($project_prefix = '', $types = array()) {

  // Let's get our FPP bundles.
  $entity_info = entity_get_info('fieldable_panels_pane');
  $bundles = $entity_info['bundles'];

  // Legacy FPPs have more configuration in code.

  $fpp = array();

  if (!empty($bundles)) {
    foreach ($bundles as $bundle_name => $bundle) {
      $fpp[$bundle_name] = array(
        'name' => $bundle['label'],
        'type' => 'fieldable_panels_pane',
        // Subtype is always the bundle for FPPs.
        'subtype' => $bundle_name,
        'description' => isset($bundle['description']) ? $bundle['description'] : '',
      );
    }
  }

  //After our FPPs we need all project related custom pane as well.
  ctools_include('content');
  $content_types = ctools_get_content_types();

  $project_content = array();

  foreach ($content_types as $type => $content_type) {
    // We only want the project related panes.
    if (strstr($type, $project_prefix) === FALSE) {
      continue;
    }
    $project_content[$type] = array(
      'name' => $content_type['title'],
      'type' => $type,
      'subtype' => $type,
      'description' => isset($content_type['description']) ? $content_type['description'] : '',
    );
  }

  return $fpp + $project_content;
}