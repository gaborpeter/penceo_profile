<h2><?php print t('Widgets'); ?></h2>
<p><?php print t('Select a widget template from the list above.'); ?></p>
<?php if (!empty($templates_rendered)): ?>
  <div class="ipe-template-list">
    <?php print implode($templates_rendered, '');?>
  </div>
  <?php else: ?>
  <p><?php print t('There are no available templates.'); ?></p>
<?php endif; ?>
