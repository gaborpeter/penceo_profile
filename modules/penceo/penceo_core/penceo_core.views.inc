<?php
/**
 * @file penceo_core.views.inc
 */


/**
 * Implements hook_views_plugins().
 */
function penceo_core_views_plugins() {
  // Have views cache the table list for us so it gets
  // cleared at the appropriate times.
  $data = views_cache_get('entity_base_tables', TRUE);
  if (!empty($data->data)) {
    $base_tables = $data->data;
  }
  else {
    $base_tables = array();
    foreach (views_fetch_data() as $table => $data) {
      if (!empty($data['table']['entity type']) && !empty($data['table']['base'])) {
        $base_tables[] = $table;
      }
    }
    views_cache_set('entity_base_tables', $base_tables, TRUE);
  }
  if (!empty($base_tables)) {
    return array(
      'module' => 'penceo_core',
      'row' => array(
        'penceo_core_multiple_view_row' => array(
          'title' => t('PENCEO: Multiple view row'),
          'help' => t('This row plugin allows rows to be displayed with different layouts.'),
          'handler' => 'penceo_core_multiple_view_row_plugin',
          'uses fields' => FALSE,
          'uses options' => TRUE,
          'type' => 'normal',
          'base' => $base_tables,
        ),
      ),
    );
  }

}