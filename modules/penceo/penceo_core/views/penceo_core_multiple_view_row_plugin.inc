<?php

/**
 * @file
 * Contains the asoif_core multiple view row plugin.
 */

/**
 * Asoif_core_multiple_view_row_plugin.
 *
 * @class penceo_core_multiple_view_row_plugin
 *
 * @extends views_plugin_row
 */
class penceo_core_multiple_view_row_plugin extends views_plugin_row {

  protected $entity_type;
  protected $entities;

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);

    // Initialize the entity-type used.
    $table_data = views_fetch_data($this->view->base_table);
    $this->entity_type = $table_data['table']['entity type'];
    // Set base table and field information as used by views_plugin_row to
    // select the entity id if used with default query class.
    $info = entity_get_info($this->entity_type);
    if (!empty($info['base table']) && $info['base table'] == $this->view->base_table) {
      $this->base_table = $info['base table'];
      $this->base_field = $info['entity keys']['id'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['view_mode'] = array('default' => 'full');
    $options['render_mode'] = array('default' => 'default');
    $options['special_view_mode_settings']['respect_pager'] = array('default' => 0);
    $options['special_view_mode_settings']['render_offset'] = array('default' => '');
    $options['special_view_mode_settings']['parity'] = array('default' => 'none');
    $options['special_view_mode_settings']['view_modes'] = array('default' => '');
    $options['special_view_mode_settings']['view_modes_number'] = array('default' => '');
    $options['special_view_mode_settings']['view_mode_options'] = array();

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {

    // Let's get the available view modes.
    $entity_info = entity_get_info($this->entity_type);
    $view_mode_options = array();

    foreach ($entity_info['view modes'] as $key => $value) {
      $view_mode_options[$key] = $value['label'];
    }

    if (count($view_mode_options) > 1) {
      $form['view_mode'] = array(
        '#type' => 'select',
        '#options' => $view_mode_options,
        '#title' => t('Default View mode'),
        '#description' => t('Default view mode for all entities.'),
        '#default_value' => $this->options['view_mode'],
      );
    }
    else {
      $form['view_mode_info'] = array(
        '#type' => 'item',
        '#title' => t('View mode'),
        '#description' => t('Only one view mode is available for this entity type.'),
        '#markup' => $view_mode_options ? current($view_mode_options) : t('Default'),
      );
      $form['view_mode'] = array(
        '#type' => 'value',
        '#value' => $view_mode_options ? key($view_mode_options) : 'default',
      );
    }

    $form['render_mode'] = array(
      '#type' => 'select',
      '#title' => t('Render mode'),
      '#description' => t('Select the render mode. You can choose from the following values: <ul>
        <li><strong>Default:</strong>&nbsp;No special setting applied.</li>
        <li><strong>Selective:</strong>&nbsp;You can select how many items should have a different view.</li>
        </ul>
      '),
      '#options' => array(
        'default' => t('Default'),
        'selective' => t('Selective')
      ),
      '#default_value' => $this->options['render_mode'],
    );

    $form['special_view_mode_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Special view mode settings'),
      '#description' => t('Here you can set how you wish your rows to be rendered.'),
      '#states' => array(
        'invisible' => array(
          ':input[name="row_options[render_mode]"]' => array('value' => 'default'),
        ),
      ),
    );

    $view_mode_number = $this->options['special_view_mode_settings']['view_modes_number'];

    $form['special_view_mode_settings']['view_modes_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of view modes'),
      '#description' => t('Give the number of view modes that will be used. e.g.: 3 if the layout consists of 3 different render modes.<br>Parity mode only exists when the number of view modes are even.'),
      '#default_value' => $this->options['special_view_mode_settings']['view_modes_number'],
      '#size' => 5,
    );

    $form['special_view_mode_settings']['view_modes_generate'] = array(
      '#type' => 'submit',
      '#value' => t('Regenerate view mode settings'),
      '#submit' => array('_penceo_core_multiple_view_row_generate'),
    );

    if ($view_mode_number > 0) {
      $form['special_view_mode_settings']['respect_pager'] = array(
        '#type' => 'checkbox',
        '#title' => t('Respect pager'),
        '#description' => t('When checked the pager results will be respected and the special settings will be applied on every page not just the first page.'),
        '#default_value' => $this->options['special_view_mode_settings']['respect_pager'],
      );

      $form['special_view_mode_settings']['parity'] = array(
        '#type' => 'select',
        '#title' => t('Parity'),
        '#description' => t('If you select the parity all your settings below will only apply when the parity validation is passed.<br>For instance if you set the first element to have a different view mode and parity is set to <i>odd</i> it will only be applied when the number of results (n) n % 2 != 0!'),
        '#options' => array(
          'none' => t('None'),
          'odd' => t('Odd'),
          'even' => t('Even'),
        ),
        '#default_value' => $this->options['special_view_mode_settings']['parity'],
      );

      // Render view mode options.
      for ($i = 1; $i <= $this->options['special_view_mode_settings']['view_modes_number']; ++$i) {
        $view_mode_key = "view_mode_{$i}";

        $form['special_view_mode_settings'][$view_mode_key] = array(
          '#type' => 'fieldset',
          '#title' => t('View mode @num', array('@num' => $i)),
        );

        $form['special_view_mode_settings'][$view_mode_key]['render_offset'] = array(
          '#type' => 'textfield',
          '#title' => t('Render offset'),
          '#description' => t('Write in the number of items you wish to be rendered differently.'),
          '#default_value' => $this->options['special_view_mode_settings'][$view_mode_key]['render_offset'],
          '#size' => 5,
        );

        $form['special_view_mode_settings'][$view_mode_key]['view_modes'] = array(
          '#type' => 'select',
          '#title' => t('View mode'),
          '#options' => $view_mode_options,
          '#default_value' => $this->options['special_view_mode_settings'][$view_mode_key]['view_modes'],
          '#states' => array(
            'invisible' => array(
              ":input[name=\"row_options[special_view_mode_settings][{$view_mode_key}][render_offset]\"]" => array(
                array('value' => ''),
                array('value' => 0),
              ),
            ),
          ),
        );
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function pre_render($values) {
    if (!empty($values)) {
      list($this->entity_type, $this->entities) = $this->view->query->get_result_entities($values, !empty($this->relationship) ? $this->relationship : NULL, isset($this->field_alias) ? $this->field_alias : NULL);
    }
    // Render the entities.
    if ($this->entities) {

      $render = array();

      switch ($this->options['render_mode']) {

        // Selective mode selected. We have to respect special cases.
        case 'selective':

          static $current_page, $items_per_page, $page_results;

          if (!isset($current_page)) {
            $current_page = $this->view->get_current_page() ? $this->view->get_current_page() : 0;
          }

          if (!isset($page_results)) {
            $page_results = count($this->view->result);
          }

          if (!isset($items_per_page)) {
            $items_per_page = $this->view->get_items_per_page();
            // When this is 0 we have no pagers set. Let's use the number
            // of results as $items_per_page.
            if (!$items_per_page) {
              $items_per_page = $page_results;
            }
          }

          $settings = $this->options['special_view_mode_settings'];

          $parity = $settings['parity'];
          $respect_pager = $settings['respect_pager'];

          // Enables special behavior.
          $enable_special_settings = FALSE;
          // There are certain cases there special settings should be skipped.
          $skip_special_settings = FALSE;

          // Parity check.
          switch ($parity) {
            case 'odd':
              if ($page_results % 2 != 0) {
                $enable_special_settings = TRUE;
              }
              break;

            case 'even':
              if ($page_results % 2 === 0) {
                $enable_special_settings = TRUE;
              }
              break;

            case 'none':
              $enable_special_settings = TRUE;
              break;
          }

          // When respect pager is checked we have to respect
          // the special view modes on all page otherwise just the first page.
          if (!$respect_pager) {
            if ($current_page > 0) {
              $skip_special_settings = TRUE;
            }
          }

          // The render array that will be passed for render.
          $modified_render[$this->entity_type] = array();
          // Stores keys that are modified.
          $modified_entities = array();

          if (!$skip_special_settings && $enable_special_settings) {
            // Preprocess different view modes.
            if ($settings['view_modes_number'] > 0) {

              // Tracks current place in order.
              $element_key = 0;

              for ($i = 1; $i <= $settings['view_modes_number']; ++$i) {
                $view_mode_element = $settings["view_mode_{$i}"];
                $render_offset = $view_mode_element['render_offset'];
                $view_mode = $view_mode_element['view_modes'];

                // We render the amount of elements set in render offset
                // with a different view mode everything else will be rendered
                // with the default view mode.
                if ($render_offset && $render_offset > 0) {
                  // Preparing elements that have a different layout.
                  if ($render_offset <= $page_results) {
                    $modified_group = array();

                    for ($j = 0; $j < $render_offset; ++$j, ++$element_key) {
                      $modified_group[$element_key] = $element_key;
                      // Track all modified keys.
                      $modified_entities[$element_key] = $element_key;
                    }

                    // Render elements that have different view mode.
                    $render_elements = entity_view($this->entity_type, array_intersect_key($this->entities, $modified_group), $view_mode);
                    // Remove it temporary we will add it later.
                    $sorted = $render_elements[$this->entity_type]['#sorted'];
                    unset($render_elements[$this->entity_type]['#sorted']);
                    // Add it to the modified elements.
                    $modified_render[$this->entity_type] += $render_elements[$this->entity_type];

                  }
                  // Fallback
                  else {
                    $render = entity_view($this->entity_type, $this->entities, $this->options['view_mode']);
                    break;
                  }
                }
              }
              // When there are remaining items we should render them too.
              if (count($modified_entities) < $page_results) {
                // Render the remaining elements with default view mode.
                $default_render = entity_view($this->entity_type, array_intersect_key($this->entities, array_diff(array_keys($this->entities), $modified_entities)), $this->options['view_mode']);
                // Merge modified and default elements.
                $render[$this->entity_type] = $modified_render[$this->entity_type] + $default_render[$this->entity_type];
              }
              else {
                // The #sorted key is missing from the render array
                // it has to be added again.
                $modified_render[$this->entity_type]['#sorted'] = isset($sorted) ? $sorted : TRUE;
                $render = $modified_render;
              }
            }
            else {
              $render = entity_view($this->entity_type, $this->entities, $this->options['view_mode']);
            }
          }
          else {
            $render = entity_view($this->entity_type, $this->entities, $this->options['view_mode']);
          }

          break;

        // Default behavior, no special settings simply render.
        default:
          $render = entity_view($this->entity_type, $this->entities, $this->options['view_mode']);
          break;
      }

      // Remove the first level of the render array.
      $this->rendered_content = reset($render);
    }
  }

  /**
   * Overridden to return the entity object.
   */
  public function get_value($values, $field = NULL) {
    return isset($this->entities[$this->view->row_index]) ? $this->entities[$this->view->row_index] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if ($entity = $this->get_value($values)) {
      $render = $this->rendered_content[entity_id($this->entity_type, $entity)];
      return drupal_render($render);
    }
  }

}

/**
 * Override handler for views_ui_edit_display_form
 */
function _penceo_core_multiple_view_row_generate($form, &$form_state) {
  $display = &$form_state['view']->display[$form_state['display_id']];
  $display->handler->options_submit($form, $form_state);

  views_ui_cache_set($form_state['view']);
  $form_state['rerender'] = TRUE;
  $form_state['rebuild'] = TRUE;
}
