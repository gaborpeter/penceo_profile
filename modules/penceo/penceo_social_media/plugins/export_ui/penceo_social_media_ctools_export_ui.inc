<?php
/**
 * @file
 * Define this Export UI plugin.
 */

$plugin = array(
  'schema' => 'penceo_social_media',
  'access' => 'administer penceo social media',
  // Define the menu item.
  'menu' => array(
    'menu item' => 'export',
    'menu prefix' => 'admin/config/services/penceo-social-media',
    'menu title' => 'Export',
    'menu description' => 'Administer Penceo Social Media presets.',
  ),

  // Define user interface texts.
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('Penceo Social Media preset'),
  'title plural proper' => t('Penceo Social Media presets'),


  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'penceo_social_media_ctools_export_ui_form',
  ),
);

/**
 * Define the preset add/edit form.
 */
function penceo_social_media_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];

  $form['penceo_social_media_export'] = array(
    '#type' => 'textfield',
    '#title' => t('Penceo Social Media configuration'),
    '#description' => t('Ctools export configuration for Penceo Social Media data'),
    '#default_value' => $preset->penceo_social_media_data,
    '#required' => TRUE,
  );
}

