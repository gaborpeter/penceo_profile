<?php
/**
 * @file
 * Hook callback helper functions for custom field settings.
 */

/**
 * Helper function for hook_field_info().
 */
function _penceo_social_media_field_info() {
  $return['penceo_followers'] = array(
    'label' => t('Penceo Social Media: followers'),
    'description' => t('Compound field for Penceo Social Media: followers.'),
    'default_widget' => 'penceo_social_media_followers_form',
    'default_formatter' => 'penceo_social_media_followers_default',
    'settings' => array('max_length' => 255),
    'instance_settings' => array('text_processing' => 0),
  );

  return $return;
}

/**
 * Helper function for hook_field_settings_form().
 */
function _penceo_social_media_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];

  $form = array();

  if ($field['type'] == 'penceo_followers') {
    $form['max_length'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum length'),
      '#default_value' => $settings['max_length'],
      '#required' => TRUE,
      '#description' => t('The maximum length of the field in characters.'),
      '#element_validate' => array('element_validate_integer_positive'),
      '#disabled' => $has_data,
    );
  }

  return $form;
}

/**
 * Helper function for hook_field_validate().
 *
 * Possible error codes:
 * - 'penceo_social_media_value_max_length': The value exceeds the maximum length.
 */
function _penceo_social_media_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    // @todo Length is counted separately for summary and value, so the maximum
    //   length can be exceeded very easily.
    foreach (array('counter_suffix', 'button_label') as $column) {
      if (!empty($item[$column])) {
        if (!empty($field['settings']['max_length']) && drupal_strlen($item[$column]) > $field['settings']['max_length']) {
          switch ($column) {
            case 'counter_suffix':
              $message = t('%name: the suffix may not be longer than %max characters.', array('%name' => $instance['label'], '%max' => $field['settings']['max_length']));
              break;

            case 'button_label':
              $message = t('%name: the button label may not be longer than %max characters.', array('%name' => $instance['label'], '%max' => $field['settings']['max_length']));
              break;
          }
          $errors[$field['field_name']][$langcode][$delta][] = array(
            'error' => "penceo_social_media_{$column}_length",
            'message' => $message,
          );
        }
      }
    }
  }
}

/**
 * Helper function for hook_field_is_empty().
 */
function _penceo_social_media_field_is_empty($item, $field) {
  return !isset($item['fsmid']) || $item['fsmid'] === '';
}

/**
 * Implements hook_field_formatter_info().
 */
function _penceo_social_media_field_formatter_info() {
  return array(
    'penceo_social_media_followers_default' => array(
      'label' => t('Default'),
      'field types' => array('penceo_followers'),
    ),
  );
}

/**
 * Helper function for hook_field_formatter_view().
 */
function _penceo_social_media_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'penceo_social_media_followers_default':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'penceo_followers_item',
          '#fsmid' => $item['fsmid'],
          '#counter_suffix' => $item['suffix'],
          '#button_label' => $item['button_label'],
        );
      }
      break;
  }
  return $element;
}

/**
 * Helper function for hook_field_widget_info().
 */
function _penceo_social_media_field_widget_info() {
  return array(
    'penceo_social_media_followers_form' => array(
      'label' => t('Penceo Social Media followers field'),
      'field types' => array('penceo_followers'),
      'settings' => array('size' => 60),
    ),
  );
}

/**
 * Helper function for hook_field_widget_settings_form().
 */
function _penceo_social_media_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  if ($widget['type'] == 'penceo_social_media_followers_form') {
    $form['size'] = array(
      '#type' => 'textfield',
      '#title' => t('Size of textfields'),
      '#default_value' => $settings['size'],
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
    );
  }

  return $form;
}

/**
 * Helper function for hook_field_widget_form().
 */
function _penceo_social_media_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case 'penceo_social_media_followers_form':
      // Create a select options list from available accounts.
      $accounts = penceo_social_media_load_accounts();
      $options = array();
      foreach ($accounts as $account) {
        $label = check_plain($account->username) . ' (' . check_plain($account->service_name) . ')';
        $options[$account->fsmid] = $label;
      }
      $element['fsmid'] = array(
        '#type' => 'select',
        '#title' => t('Social Media Accounts'),
        '#options' => $options,
        '#default_value' => isset($items[$delta]['fsmid']) ? $items[$delta]['fsmid'] : NULL,
        '#empty_value' => '',
        '#empty_option' => t('-- None --'),
        '#required' => TRUE,
      );

      $element['suffix'] = array(
        '#type' => 'textfield',
        '#title' => t('Suffix'),
        '#default_value' => isset($items[$delta]['suffix']) ? $items[$delta]['suffix'] : NULL,
        '#size' => $instance['widget']['settings']['size'],
        '#maxlength' => $field['settings']['max_length'],
      );

      $element['button_label'] = array(
        '#type' => 'textfield',
        '#title' => t('Button label'),
        '#default_value' => isset($items[$delta]['button_label']) ? $items[$delta]['button_label'] : NULL,
        '#size' => $instance['widget']['settings']['size'],
        '#maxlength' => $field['settings']['max_length'],
      );
      break;
  }

  return $element;
}
