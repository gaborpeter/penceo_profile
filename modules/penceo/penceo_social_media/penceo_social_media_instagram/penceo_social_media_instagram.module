<?php

/**
 * @file
 * Penceo Social Media for instagram
 */

include_once 'instagram.inc';
use MetzWeb\Instagram\Instagram;

/**
 * Implements hook_menu().
 */
function penceo_social_media_instagram_menu() {
  $items = array();

  $items['admin/config/services/penceo-social-media/instagram'] = array(
    'title' => 'Instagram settings',
    'description' => 'Penceo: Social Media - Instagram settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('penceo_social_media_instagram_settings_form'),
    'access arguments' => array('administer penceo social media'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_penceo_social_media_provider().
 */
function penceo_social_media_instagram_penceo_social_media_provider() {
  return array(
    'instagram' => array(
      'machine_name' => 'instagram',
      'label' => 'Instagram',
      'callback_counter_function' => 'penceo_social_media_instagram_count',
      'callback_url_function' => 'penceo_social_media_instagram_url',
    ),
  );
}

/**
 * Implements form callback.
 */
function penceo_social_media_instagram_settings_form($form, &$form_state) {
  $client_id = variable_get('penceo_instagram_client_id', '');
  $client_secret = variable_get('penceo_instagram_client_secret', '');
  $access_token = variable_get('penceo_instagram_auth_code', '');

  if (!empty($client_id) && !empty($client_secret)) {
    $instagram = penceo_social_media_instagram_get_instagram_api();
    $needs_auth = false;

    if (empty($access_token)) {
      $needs_auth = true;
    }

    if (!empty($access_token)) {
      $user_data = $instagram->getUser();
      if ($user_data->meta->code != 200) {
        $needs_auth = true;
      }
    }

    if ($needs_auth) {
      $login_url = $instagram->getLoginUrl(array('basic', 'public_content'));
      drupal_set_message("You need to OAuth, please " . l('click here', $login_url));
    }

    /* Saving incoming access token from instagram */
    if (isset($_GET['code'])) {
      $code = $_GET['code'];
      $oauth = $instagram->getOAuthToken($code);
      variable_set('penceo_instagram_auth_code', $oauth->access_token);
      drupal_set_message("Instagram access token updated with {$oauth->access_token}!");
    }
  }

  $form['penceo_instagram_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Instagram Client ID'),
    '#default_value' => $client_id,
    '#required' => TRUE,
  );
  $form['penceo_instagram_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Instagram Client Secret'),
    '#default_value' => $client_secret,
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Fetch the likes of a instagram page.
 */
function penceo_social_media_instagram_count($username) {
  $instagram = penceo_social_media_instagram_get_instagram_api();

  $users = $instagram->searchUser($username, 1);
  $user = $instagram->getUser($users->data[0]->id);

  return $user->data->counts->followed_by;
}

/**
 * Generate user profile page url.
 */
function penceo_social_media_instagram_url($username) {
  return "https://www.instagram.com/{$username}";
}

/**
 * Implements hook_cronapi().
 */
function penceo_social_media_instagram_cronapi($op, $job = NULL) {
  $items['penceo_social_media_instagram_auth_token_check'] = array(
    'description' => 'Checks if the auth token is still working',
    'rule' => '0 0 * * *',
    'callback' => 'penceo_social_media_instagram_auth_token_check_callback',
  );

  return $items;
}

/**
 * Function to check if the auth token is still working.
 */
function penceo_social_media_instagram_auth_token_check_callback() {
  $instagram = penceo_social_media_instagram_get_instagram_api();
  $user_data = $instagram->getUser();
  if ($user_data->meta->code != 200) {
    drupal_mail('penceo_social_media_instagram', 'access_token_expired_warning', '');
  }
}

/**
 * Returns the initialized instagram objects and tries to set auth token.
 * @return \MetzWeb\Instagram\Instagram
 */
function penceo_social_media_instagram_get_instagram_api() {
  global $base_url;

  $instagram = new Instagram(array(
    'apiKey'      => variable_get('penceo_instagram_client_id', ''),
    'apiSecret'   => variable_get('penceo_instagram_client_secret', ''),
    'apiCallback' => "{$base_url}/admin/config/services/penceo-social-media/instagram",
  ));

  $access_token = variable_get('penceo_instagram_auth_code', '');

  if (!empty($access_token)) {
    $instagram->setAccessToken($access_token);
  }

  return $instagram;
}

/**
 * Implements hook_mail().
 */
function penceo_social_media_instagram_mail($key, &$message, $params) {
  global $base_url;

  switch ($key) {
    case 'access_token_expired_warning':
      $message['to'] = variable_get("site_tech_admin_mails", 'gabor@penceo.com, adam@penceo.com');
      $message['subject'] = t('Instagram access token expired on site: @site', array('@site' => variable_get('site_name', 'Penceo')));
      $message['body'][] = t('THe Instagram access token expired on the website, please go and re-authorize here: @url', array('@url' => url("{$base_url}/admin/config/services/penceo-social-media/instagram")));
      break;
  }
}
