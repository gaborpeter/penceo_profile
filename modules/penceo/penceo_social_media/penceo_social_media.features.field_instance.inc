<?php
/**
 * @file
 * penceo_social_media.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function penceo_social_media_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'fieldable_panels_pane-fsm_followers-field_followers'
  $field_instances['fieldable_panels_pane-fsm_followers-field_followers'] = array(
    'bundle' => 'fsm_followers',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'penceo_social_media',
        'settings' => array(),
        'type' => 'penceo_social_media_followers_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_followers',
    'label' => 'Followers',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'penceo_social_media',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'penceo_social_media_followers_form',
      'weight' => -4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Background color');
  t('Background image');
  t('Followers');

  return $field_instances;
}
