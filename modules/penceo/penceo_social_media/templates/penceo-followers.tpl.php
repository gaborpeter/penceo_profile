<?php
/**
 * @file
 * Social media follow up pane.
 */
?>

<div class="social-media-follow-us <?php print drupal_html_class($service_name) ?>">
  <?php if ($provider_label): ?>
    <?php print $provider_label; ?>
  <?php endif; ?>
  <?php if ($counter): ?>
    <?php print $counter; ?>
  <?php endif; ?>
  <?php if ($follow_button): ?>
    <?php print $follow_button; ?>
  <?php endif; ?>
</div>
