<?php
/**
 * @file
 * penceo_social_media.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function penceo_social_media_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_followers'
  $field_bases['field_followers'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_followers',
    'foreign keys' => array(
      'fsmid' => array(
        'columns' => array(
          'fsmid' => 'fsmid',
        ),
        'table' => 'penceo_social_media',
      ),
    ),
    'indexes' => array(
      'fsmid' => array(
        0 => 'fsmid',
      ),
    ),
    'locked' => 0,
    'module' => 'penceo_social_media',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'penceo_followers',
  );

  return $field_bases;
}
