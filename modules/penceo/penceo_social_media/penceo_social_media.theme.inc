<?php
/**
 * @file
 * Contains custom theme callbacks.
 */

/**
 * Theme function for service label.
 */
function theme_penceo_followers_service_label($variables) {
  $output = array();
  $service_name = $variables['service_name'];

  $output['provider_label'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'social-media-service',
        drupal_html_class($service_name),
      ),
    ),
  );

  return drupal_render($output);
}

/**
 * Theme function for counter.
 */
function theme_penceo_followers_service_counter($variables) {
  $counter = check_plain($variables['counter']);
  $suffix = check_plain($variables['counter_suffix']);
  $output = array();
  $formatted_counter = number_format((int)$counter, 0, '', ' ');
  $output['service_counter'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('social-media-followers'),
    ),
    'counter' => array(
      '#markup' => "<div class='counter'>{$formatted_counter}</div><div class='suffix'>{$suffix}</div> ",
    ),
  );
  return drupal_render($output);
}

/**
 * Theme function for counter.
 */
function theme_penceo_followers_service_button($variables) {
  $button_label = $variables['button_label'];
  $url = $variables['url'];

  $output = array();
  $output['service_follow_button'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('social-media-follow-button'),
    ),
    'counter' => array(
      '#markup' => l($button_label, $url, array(
        'attributes' => array(
          'class' => array('rounded-btn', 'text'),
          'target' => '_blank',
        ),
      )),
    ),
  );
  return drupal_render($output);
}
