<?php
/**
 * @file
 * Contains admin function of the Social Media configuration.
 */

/**
 * Implements custom form callback for social media settings.
 */
function penceo_social_media_settings_form($form, &$form_state) {
  $form['registered_services']['#markup'] = penceo_social_media_registered_services_table();
  $form['service_name'] = array(
    '#type' => 'select',
    '#title' => t('Social Media service name'),
    '#options' => penceo_social_media_providers_option_list(),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Social Media account'),
  );
  return $form;
}

/**
 * Submit function of the Social Media admin form.
 */
function penceo_social_media_settings_form_submit(&$form, &$form_state) {
  db_insert('penceo_social_media')
    ->fields(array(
      'service_name' => $form_state['values']['service_name'],
      'username' => $form_state['values']['username'],
    ))
    ->execute();
}

/**
 * Generate a select options list from social media providers.
 */
function penceo_social_media_providers_option_list() {
  $return = array();
  $providers = penceo_social_media_providers();
  foreach ($providers as $provider) {
    $return[$provider['machine_name']] = $provider['label'];
  }
  return $return;
}

/**
 * Helper function for registered accounts.
 */
function penceo_social_media_registered_services_table() {
  $results = db_select('penceo_social_media', 'fsm')
    ->fields('fsm')
    ->execute()
    ->fetchAll();

  $header = array(
    'username' => t('Username'),
    'service_name' => t('Service name'),
    'delete' => t('Action'),
  );
  $rows = array();

  foreach ($results as $result) {
    $rows[$result->fsmid]['username'] = $result->username;
    $rows[$result->fsmid]['service_name'] = $result->service_name;
    $rows[$result->fsmid]['delete'] = l(t('Delete'), "penceo-social-media/{$result->fsmid}/delete");
  }

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('No accounts available.')));
}

/**
 * Custom confirm form for social media account deletion.
 */
function penceo_social_media_account_delete_confirm($form, &$form_state, $fsmid) {
  $penceo_social_media_account = penceo_social_media_load_account($fsmid);
  // Always provide entity id in the same form key as in the entity edit form.
  $form['fsmid'] = array('#type' => 'value', '#value' => $fsmid);
  return confirm_form($form,
    t('Are you sure you want to delete %username account from %service service?', array(
      '%username' => $penceo_social_media_account->username,
      '%service' => $penceo_social_media_account->service_name)),
      'admin/config/services/penceo-social-media',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes social media account deletion.
 */
function penceo_social_media_account_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_delete('penceo_social_media')
      ->condition('fsmid', $form_state['values']['fsmid'])
      ->execute();
  }
  drupal_set_message(t('The account has been deleted.'), 'status');
  $form_state['redirect'] = 'admin/config/services/penceo-social-media';
}
