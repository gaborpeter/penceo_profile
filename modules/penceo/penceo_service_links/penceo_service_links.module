<?php
/**
 * @file
 * Contains Service links customization.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function penceo_service_links_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && $plugin_type == 'content_types') {
    return 'plugins/' . $plugin_type;
  }
}

/**
 * Implements hook_theme().
 */
function penceo_service_links_theme($existing, $type, $theme, $path) {
  return array(
    'penceo_share' => array(
      'variables' => array('entity' => null),
    ),
  );
}

/**
 * Build a single link following the style rules.
 * (Share counter is added to the output for icon+text style.)
 */
function penceo_service_links_service_links_build_link($variables) {
  $text = $variables['text'];
  $url = $variables['url'];
  $image = $variables['image'];
  $nodelink = $variables['nodelink'];
  $style = $variables['style'];
  $attributes = $variables['attributes'];
  if ($nodelink) {
    $query = isset($url[1]) ? $url[1] : NULL;
 
    switch ($style) {
      case SERVICE_LINKS_STYLE_TEXT:
        $link = array(
          'title' => $text,
          'href' => $url[0],
          'query' => $query,
          'attributes' => $attributes,
        );
        break;
      case SERVICE_LINKS_STYLE_IMAGE:
        $alt_text = t('!name logo', array('!name' => $text));
        $link = array(
          'title' => '<span class="icon">' . theme('image', array('path' => service_links_expand_path($image), 'alt' => $alt_text)) . '</span>',
          'href' => $url[0],
          'query' => $query,
          'attributes' => $attributes,
          'html' => TRUE,
        );
        break;
      case SERVICE_LINKS_STYLE_IMAGE_AND_TEXT:
        // Share counter is added to the output.
        // Text is replaced by share counter in JS.
        $text = '0';
        $alt_text = t('!name logo', array('!name' => $text));
        $link = array(
          'title' => '<span class="icon">' . theme('image', array('path' => service_links_expand_path($image), 'alt' => $alt_text)) . '</span> <span class="count">'. $text . '</span>',
          'href' => $url[0],
          'query' => $query,
          'attributes' => $attributes,
          'html' => TRUE,
        );
        break;
      case SERVICE_LINKS_STYLE_EMPTY:
        $link = array(
          'title' => '<span class="element-invisible">' . $text . '</span>',
          'href' => $url[0],
          'query' => $query,
          'attributes' => $attributes,
          'html' => TRUE,
        );
        break;
      default:
        $link = theme($style, $variables);
    }
  }
  else {
    $attributes = array('attributes' => $attributes);
    if (isset($url[1])) {
      $attributes['query'] = $url[1];
    }

    switch ($style) {
      case SERVICE_LINKS_STYLE_TEXT:
        $link = l($text, $url[0], $attributes);
        break;
      case SERVICE_LINKS_STYLE_IMAGE:
        $attributes['html'] = TRUE;
        $alt_text = t('!name logo', array('!name' => $text));
        $link = l('<span class="icon">' . theme('image', array('path' => service_links_expand_path($image), 'alt' => $alt_text)) . '</span>', $url[0], $attributes);
        break;
      case SERVICE_LINKS_STYLE_IMAGE_AND_TEXT:
        // Share counter is added to the output.
        // Text is replaced by share counter in JS.
        $attributes['html'] = TRUE;
        $alt_text = t('!name logo', array('!name' => $text));
        $link = l('<span class="icon">' . theme('image', array('path' => service_links_expand_path($image), 'alt' => $alt_text)) . '</span> <span class="text">'. $text . '</span>', $url[0], $attributes);
        break;
      case SERVICE_LINKS_STYLE_EMPTY:
        $attributes['html'] = TRUE;
        $link = l('<span class="element-invisible">' . $text . '</span>', $url[0], $attributes);
        break;
      case SERVICE_LINKS_STYLE_FISHEYE:
        $attributes['attributes']['class'] = isset($attributes['attributes']['class']) ? array_merge($attributes['attributes']['class'], array('fisheyeItem')) : array('fisheyeItem');
        $attributes['html'] = TRUE;
        $link = l(theme('image', array('path' => service_links_expand_path($image, 'fisheye'), 'alt' => $text, 'getsize' => FALSE)) .'<span>'. $text .'</span>', $url[0], $attributes);
        break;
      default:
        $link = theme($style, $variables);
    }
  }

  return $link;
}

/**
 * Custom theme function for share buttons.
 */
function theme_penceo_share($variables) {
  $entity = $variables['entity'];
  $options = NULL;
  if (isset($variables['options'])) {
    $options = $variables['options'];
  }
  $path = drupal_get_path('module', 'penceo_service_links');
  // Don't load JS on admin pages.
  if (strpos($_GET['q'], 'admin') !== 0) {
    drupal_add_js("{$path}/js/share_button.js");
  }
  
  if (isset($entity->nid)) {
    $entity_type = 'node';
    $service_links = service_links_render($entity, TRUE, $options);
    $label = ' ';
  }
  else {
    $service_links = service_links_render(NULL, TRUE, $options);
    $entity_type = $label = NULL;
  }
  return theme('service_links_node_format', array(
    'links' => $service_links,
    'label' => $label,
    'view_mode' => 'full',
    'node_type' => $entity_type,
  ));
}


function penceo_service_links_theme_registry_alter(&$theme_registry) {
  unset($theme_registry['service_links_build_link']['file']);
  $theme_registry['service_links_build_link']['theme path'] = drupal_get_path('module', 'penceo_service_links');
  $theme_registry['service_links_build_link']['function'] = 'penceo_service_links_service_links_build_link';
  $theme_registry['service_links_build_link']['variables']['style'] = SERVICE_LINKS_STYLE_IMAGE;
}

/**
 * Implements hook_service_links_alter().
 * Allows alteration of the Service Links.
 *
 * @param $links
 *   The constructed array of service links.
 */
function penceo_service_links_service_links_alter(&$links) {
  foreach ($links as &$link) {
    // Removing Service link javascript files:
    if (isset($link['javascript'])) {
      unset($link['javascript']);
    }
  }
  $icon_overrides = array(
    'facebook' => 'facebook.png',
    'facebook_share' => 'facebook.png',
    'facebook_like' => 'facebook.png',
    'google_plus' => 'google_plus.png',
    'google_plus_one' => 'google_plus.png',
    'twitter' => 'twitter.png',
    'twitter_widget' => 'twitter.png',
    'linkedin' => 'linkedin.png',
    'linkedin_share_button' => 'linkedin.png',
  );
  $path = drupal_get_path('module', 'penceo_service_links');
  // Changing icons:
  foreach ($icon_overrides as $service_name => $icon) {
    if (isset($links[$service_name])) {
      $links[$service_name]['icon'] = "{$path}/images/social_icons/{$icon}";
      $links[$service_name]['style'] = SERVICE_LINKS_STYLE_IMAGE;
    }
  }
}
