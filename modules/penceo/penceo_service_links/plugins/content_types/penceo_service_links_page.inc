<?php

/**
 * @file
 * Plugin for handling the 'penceo_service_links' content type which allows the
 * logo of the site to be embedded into a panel.
 */
/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Penceo Service Links - for custom pages'),
  'single' => TRUE,
  'icon' => 'icon_node.png',
  'description' => t('Customized Service Links by Penceo.'),
  'category' => t('EGA'),
  'render callback' => 'penceo_service_links_page_content_type_render',
  'all contexts' => TRUE,
);

/**
 * Output function for the 'page_logo' content type.
 *
 * Outputs the logo for the current page.
 */
function penceo_service_links_page_content_type_render($subtype, $conf, $panel_args, $contexts) {
  $block = new stdClass();
  $block->title = '';
  $block->content = theme('penceo_share', array('options' => array('style' => SERVICE_LINKS_STYLE_IMAGE)));
  return $block;
}
