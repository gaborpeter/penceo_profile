(function ($) {
  Drupal.ShareButtons = Drupal.ShareButtons || {};

  Drupal.behaviors.ShareButtons = {
    attach: function (context, settings) {
      var share = function() {
        var _this = this;

        this.defaultConfig = {
          settings: {
            showCls: 'show',
            hasGivenDirectionCls: 'has-given-direction',
            defaultDirection: 'left',
            openDirections: [
              {
              	wrapper: '.ega-node-highlighted, .ega-node-teaser',
              	breakpoint: {
              		0: {
              			directions: ['right']
              		}
              	}
              },
              {
                wrapper: '.media-feed-layout',
                breakpoint: {
                  0: {
                    directions: ['right']
                  }
                }
              }
            ]
          },
          selectors: {
            wrapper: '.service-links',
            opener: '.service-links .service-label',
            content: '.service-links .links'
          }
        };

        this.elements;
        this.events;
        this.config;

        this.init = function() {
          this.config = this.defaultConfig;
          this.elements = selectorsToElements(this.config.selectors);
        };

        this.create = function() {
          this.init();

          this.setShareDirection();
          this._setEvents();
        };

        this.destroy = function() {
          unbind(this.events);
        };

        this.refresh = function() {
          this.elements = selectorsToElements(this.config.selectors);

          this.setShareDirection();

          this._setEvents();
        };

        this._setEvents = function() {
          this.events = [
            {
              el: this.elements.opener,
              ev: 'click.onOpenerClick',
              fn: function(e) { _this._onOpenerClick(e) }
            },
            {
              el: jQuery(window),
              ev: 'resizeEnd.onShareWindowResizeEnd',
              fn: function() { _this._onShareWindowResizeEnd() }
            },
            {
              el: jQuery(document),
              ev: 'click.onShareDocumentClick',
              fn: function(e) { _this._onShareDocumentClick(e) }
            }
          ];

          unbind(this.events);
          bind(this.events);
        };

        this._onOpenerClick = function(e) {
          e.preventDefault();

          var el = jQuery(e.currentTarget),
              wrapper = el.parents(this.config.selectors.wrapper),
              opened = wrapper.hasClass(this.config.settings.showCls);

          if (opened) {
            this.close(wrapper);
          }
          else {
            this.open(wrapper);
          }

        };

        this._onShareDocumentClick = function(e) {
          var target = jQuery(e.target);

          this.elements.wrapper.each(function() {
            var wrapper = jQuery(this),
                outerClick = isOuterClick(target, wrapper);

            if (outerClick) {
              if (wrapper.hasClass(_this.config.settings.showCls)) {
                _this.close(wrapper);
              }
            }
          });
        };

        this.open = function(wrapper) {
          var content = wrapper.find(this.elements.content),
              css = {};

          wrapper.addClass(this.config.settings.showCls);

          if (content.hasClass('up') || content.hasClass('down')) {
            css.height = getHeight(content);
          }
          else {
            css.width = getWidth(content);
          }

          content.css(css);
        };

        this.close = function(wrapper) {
          var content = wrapper.find(this.elements.content);

          wrapper.removeClass(this.config.settings.showCls);

          content.css(
              {
                height: '',
                width: ''
              }
          )
        };

        this.setShareDirection = function() {
          var openDirections = this.config.settings.openDirections;

          for (var i = 0; i < openDirections.length; i++) {
            var wrapper = openDirections[i].wrapper,
                element = jQuery(wrapper).find(this.elements.wrapper),
                content = element.find(this.elements.content),
                breakpoints = openDirections[i].breakpoint,
                viewportWidth = getViewport().width,
                directions,
                direction;

            for (var breakpoint in breakpoints) {
              if (breakpoint <= viewportWidth) {
                directions = breakpoints[breakpoint].directions;
              }
            }

            // check
            direction = directions[0];

            if (!content.hasClass(direction)) {
              content.removeClass('left right up down');

              content.addClass(direction);

            }

            if (!element.hasClass(this.config.settings.hasGivenDirectionCls)) {
              element.addClass(this.config.settings.hasGivenDirectionCls);
            }
          }

          this.elements.wrapper.each(function() {
            var wrapper = jQuery(this),
                content = wrapper.find(_this.elements.content),
                hasGivenDirection = wrapper.hasClass(_this.config.settings.hasGivenDirectionCls);

            if (!hasGivenDirection) {
              content.addClass(_this.config.settings.defaultDirection);
            }
          })
        };

        this._onShareWindowResizeEnd = function() {
          this.setShareDirection();
        };
      };

      if (window['shareInstance'] == undefined) {
        window['shareInstance'] = new share();
        window['shareInstance'].create();
      }
      else {
        window['shareInstance'].refresh();
      }
    }
  }
})(jQuery);