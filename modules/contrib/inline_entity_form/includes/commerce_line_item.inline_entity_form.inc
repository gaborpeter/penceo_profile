<?php

/**
 * @file
 * Defines the inline entity form controller for Commerce Line Items.
 */
class CommerceLineItemInlineEntityFormController extends EntityInlineEntityFormController {

  /**
   * Overrides EntityInlineEntityFormController::tableFields().
   */
  public function tableFields($bundles) {
    $fields = array();
    $fields['line_item_label'] = array(
      'type' => 'property',
      'label' => t('Label'),
      'weight' => 1,
    );
    $fields['commerce_unit_price'] = array(
      'type' => 'field',
      'label' => t('Unit price'),
      'formatter' => 'commerce_price_formatted_amount',
      'weight' => 2,
    );
    $fields['quantity'] = array(
      'type' => 'property',
      'label' => t('Quantity'),
      'weight' => 3,
    );
    $fields['commerce_total'] = array(
      'type' => 'field',
      'label' => t('Total'),
      'formatter' => 'commerce_price_formatted_amount',
      'weight' => 4,
    );

    return $fields;
  }

  /**
   * Overrides EntityInlineEntityFormController::defaultSettings().
   */
  public function defaultSettings() {
    $defaults = parent::defaultSettings();

    // Line items should always be deleted when the order is deleted, they
    // are never managed alone.
    $defaults['delete_references'] = TRUE;
    $defaults['allow_line_item_price_modification'] = TRUE;

    return $defaults;
  }

  /**
   * Overrides EntityInlineEntityFormController::settingsForm().
   */
  public function settingsForm($field, $instance) {
    $form = parent::settingsForm($field, $instance);

    // Adding existing entities is not supported for line items.
    $form['allow_existing']['#access'] = FALSE;
    $form['match_operator']['#access'] = FALSE;

    $form['allow_line_item_price_modification'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow price modification'),
      '#description' => t('Allows to set a different price for the line item (instead of the default product price).'),
      '#default_value' => $this->settings['allow_line_item_price_modification'],
    );

    return $form;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   */
  public function entityForm($entity_form, &$form_state) {
    $line_item = $entity_form['#entity'];
    $extra_fields = field_info_extra_fields('commerce_line_item', $line_item->type, 'form');

    $entity_form['line_item_details'] = array(
      '#type' => 'fieldset',
      '#title' => t('Line item details'),
      '#attributes' => array('class' => array('ief-line_item-details', 'ief-entity-fieldset')),
    );
    $entity_form['line_item_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Line item label'),
      '#description' => t('Supply the line item label to be used for this line item.'),
      '#default_value' => $line_item->line_item_label,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#weight' => $extra_fields['label']['weight'],
      '#fieldset' => 'line_item_details',
    );
    $entity_form['quantity'] = array(
      '#type' => 'textfield',
      '#datatype' => 'integer',
      '#title' => t('Quantity'),
      '#description' => t('The quantity of line items.'),
      '#default_value' => (int) $line_item->quantity,
      '#size' => 4,
      '#maxlength' => max(4, strlen($line_item->quantity)),
      '#required' => TRUE,
      '#weight' => $extra_fields['label']['weight'],
      '#fieldset' => 'line_item_details',
    );

    // If the order is still in a cart status, then the prices are still being
    // recalculated, meaning that no changes made to them would be permanent.
    if (module_exists('commerce_order') && !empty($form_state['commerce_order'])) {
      if (commerce_cart_order_is_cart($form_state['commerce_order'])) {
        $language = $entity_form['commerce_unit_price']['#language'];
        $entity_form['commerce_unit_price'][$language][0]['amount']['#disabled'] = TRUE;
        $entity_form['commerce_unit_price'][$language][0]['currency_code']['#disabled'] = TRUE;
        $entity_form['commerce_unit_price'][$language][0]['amount']['#description'] = t('Automatically calculated for shopping cart orders.');
      }
    }
    field_attach_form('commerce_line_item', $line_item, $entity_form, $form_state);

    // Tweaks specific to product line items.
    if (in_array($line_item->type, $this->productLineItemTypes())) {
      $entity_form['line_item_label']['#access'] = FALSE;
      $entity_form['commerce_display_path']['#access'] = FALSE;
      $entity_form['commerce_product']['#weight'] = -100;
      if ($this->getSetting('allow_line_item_price_modification') && !empty($entity_form['commerce_unit_price']['#access']) && empty($entity_form['#entity']->is_new)) {
        // Add option to reset the price to default:
        $entity_form['line_item_price_reset'] = array(
          '#type' => 'checkbox',
          '#title' => t('(Re-)set default price'),
          '#description' => t('(Re-)sets the unit price to the products default unit price on save. Otherwise the entered value will be used.'),
          '#default_value' => 0,
          '#weight' => $extra_fields['label']['weight'] + 99,
          '#fieldset' => 'line_item_details',
        );
        // Disable the price input if price reset is checked:
        $entity_form['commerce_unit_price']['#states'] = array(
          'disabled' => array(
            ':input[name="commerce_line_items[und][entities][' . $entity_form['#ief_row_delta'] . '][form][line_item_price_reset]"]' => array('checked' => TRUE),
          ),
        );
      }
      else {
        // Prevent price modification
        $entity_form['commerce_unit_price']['#access'] = FALSE;
      }
    }
    // Add all fields to the main fieldset.
    foreach (field_info_instances('commerce_line_item', $line_item->type) as $a => $instance) {
      $entity_form[$instance['field_name']]['#fieldset'] = 'line_item_details';
    }

    return $entity_form;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityFormValidate().
   *
   * @todo Remove once Commerce gets a quantity #element_validate function.
   */
  public function entityFormValidate($entity_form, &$form_state) {
    $line_item = $entity_form['#entity'];

    $parents_path = implode('][', $entity_form['#parents']);
    $line_item_values = drupal_array_get_nested_value($form_state['values'], $entity_form['#parents']);
    $quantity = $line_item_values['quantity'];

    if (!is_numeric($quantity) || $quantity <= 0) {
      form_set_error($parents_path . '][quantity', t('You must specify a positive number for the quantity'));
    }
    elseif ($entity_form['quantity']['#datatype'] == 'integer' &&
        (int) $quantity != $quantity) {
      form_set_error($parents_path . '][quantity', t('You must specify a whole number for the quantity.'));
    }

    field_attach_form_validate('commerce_line_item', $line_item, $entity_form, $form_state);
  }

  /**
   * Overrides EntityInlineEntityFormController::entityFormSubmit().
   */
  public function entityFormSubmit(&$entity_form, &$form_state) {
    $line_item = $entity_form['#entity'];
    $line_item_values = drupal_array_get_nested_value($form_state['values'], $entity_form['#parents']);
    if (!empty($line_item_values['quantity'])) {
      $line_item->quantity = sprintf("%.2f", $line_item_values['quantity']);
    }
    field_attach_submit('commerce_line_item', $line_item, $entity_form, $form_state);
    $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    if (in_array($line_item->type, $this->productLineItemTypes())) {
      // Populate the line item with the original product data
      commerce_product_line_item_populate($line_item, $wrapper->commerce_product->value());
      $language = $entity_form['commerce_unit_price']['#language'];
      if (isset($line_item_values['line_item_price_reset']) && $line_item_values['line_item_price_reset'] == 0 && !empty($line_item_values['commerce_unit_price'][$language][0]['amount'])) {
        // If the unit price was manually set, change the line item unit price ::
        $line_item->commerce_unit_price = $line_item_values['commerce_unit_price'];
      }
      // We have to rebase the unit price.
      commerce_line_item_rebase_unit_price($line_item);
      // Process the unit price through Rules so it reflects the user's actual
      // purchase price. (Important for VAT calculation (commerce_vat) e.g.!)
      rules_invoke_event('commerce_product_calculate_sell_price', $line_item);
    }
    else {
      $wrapper->line_item_label = trim($line_item_values['line_item_label']);
      commerce_line_item_rebase_unit_price($line_item);
    }

    // Update the total price:
    // Use the unit_price array as default
    $line_item->commerce_total = $line_item->commerce_unit_price;
    $unit_price = $wrapper->commerce_total->value();
    $wrapper->commerce_total->amount = $line_item->quantity * $unit_price['amount'];
    $wrapper->commerce_total->currency_code = $unit_price['currency_code'];
    // Add the components multiplied by the quantity to the data array.
    foreach ($unit_price['data']['components'] as $key => &$component) {
      $component['price']['amount'] *= $line_item->quantity;
    }
    // Set the updated data array to the total price.
    $wrapper->commerce_total->data = $unit_price['data'];
  }

  /**
   * Overrides EntityInlineEntityFormController::removeForm().
   */
  public function removeForm($remove_form, &$form_state) {
    // EntityInlineEntityFormController::removeForm uses the entity label
    // in the confirmation message, but line items don't have any.
    $remove_form['message'] = array(
      '#markup' => '<div>' . t('Are you sure you want to remove this line item?') . '</div>',
    );
    return $remove_form;
  }

  /**
   * Returns an array of product line item types.
   *
   * If the commerce_product_reference module is missing, returns an empty
   * array as a fallback.
   */
  protected function productLineItemTypes() {
    if (module_exists('commerce_product_reference')) {
      return commerce_product_line_item_types();
    }
    else {
      return array();
    }
  }

}
