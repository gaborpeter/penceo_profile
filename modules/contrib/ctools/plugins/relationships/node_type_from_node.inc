<?php

/**
 * @file
 * Plugin to provide an relationship handler for a node type from node.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Node type from node'),
  'keyword' => 'node_type',
  'description' => t('Adds a node type from a node context.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'context' => 'ctools_relationship_node_type_from_node_context',
);

/**
 * Return a new context based on an existing context.
 */
function ctools_relationship_node_type_from_node_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('node_type', NULL);
  }

  if (isset($context->data->type)) {
    return ctools_context_create('node_type', $context->data->type);
  }
}
