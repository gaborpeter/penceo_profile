<?php

/**
 * @file
 *
 * Plugin to provide an argument handler for a node type.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Node Type'),
  'keyword' => 'node_type',
  'description' => t('Creates a node type object from a machine-name argument.'),
  'context' => 'ctools_argument_node_type_context',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the node type for this context.'),
  ),
);

/**
 * Discover if this argument gives us the node type we crave.
 */
function ctools_argument_node_type_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  // If unset it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('node_type');
  }

  // We can accept either a node object or a pure nid.
  if (is_object($arg)) {
    return ctools_context_create('node_type', $arg);
  }

  if (!is_string($arg)) {
    return FALSE;
  }

  $node_type = node_type_get_type($arg);
  if (!$node_type) {
    return FALSE;
  }

  return ctools_context_create('node_type', $node_type);
}
