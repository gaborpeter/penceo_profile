<?php

/**
 * @file
 *
 * Plugin to provide a node type context.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Node type'),
  'description' => t('A node type object.'),
  'context' => 'ctools_context_create_node_type',
  'edit form' => 'ctools_context_node_type_settings_form',
  'defaults' => array('type' => ''),
  'keyword' => 'node_type',
  'context name' => 'node_type',
  'convert list' => 'ctools_context_node_type_convert_list',
  'convert' => 'ctools_context_node_type_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the node type for this context.'),
  ),
);

/**
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function ctools_context_create_node_type($empty, $data = NULL, $conf = FALSE, $plugin) {
  $context = new ctools_context('node_type');
  $context->plugin = 'node_type';

  if ($empty) {
    return $context;
  }

  // Load from config, if available.
  if ($conf && is_array($data)) {
    $type = $data['type'];
    $data = node_type_get_type($type);
  }

  // Load from string.
  elseif (is_string($data)) {
    $type = $data;
    $data = node_type_get_type($type);
  }

  // No loading necessary, just extract the type.
  elseif (is_object($data)) {
    $type = $data->type;
  }

  if (!empty($data)) {
    $context->data = $data;
    $context->argument = $type;

    return $context;
  }
}

/**
 * Settings form callback.
 */
function ctools_context_node_type_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['type'] = array(
    '#title' => t('Node type'),
    '#type' => 'select',
    '#options' => node_type_get_names(),
    '#default_value' => $conf['type'],
    '#required' => TRUE,
    '#description' => t('Select the node type for this context.'),
  );

  return $form;
}

/**
 * Settings form submit callback.
 */
function ctools_context_node_type_settings_form_submit($form, &$form_state) {
  $form_state['conf']['type'] = $form_state['values']['type'];
}

/**
 * Convert list callback.
 */
function ctools_context_node_type_convert_list($plugin) {
  return array(
    'name' => t('Human-readable name'),
    'base' => t('Base string'),
    'description' => t('Description'),
    'type' => t('Machine-readable name'),
    'help' => t('Help information'),
    'title_label' => t('Title label'),
    'module' => t('Module'),
    'orig_type' => t('Original type'),
  );
}

/**
 * Convert callback.
 */
function ctools_context_node_type_convert($context, $converter, $options = array()) {
  if (!empty($context->data)) {
    switch ($converter) {
      case 'name':
        return $context->data->name;
      case 'base':
        return $context->data->base;
      case 'description':
        return $context->data->description;
      case 'type':
        return $context->data->type;
      case 'help':
        return $context->data->help;
      case 'title_label':
        return $context->data->title_label;
      case 'module':
        return $context->data->module;
      case 'orig_type':
        return $context->data->orig_type;
    }
  }
}
