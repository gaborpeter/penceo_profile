https://www.drupal.org/node/1201840

This patch gives an additional argument/context/relationship to ctools for node_types.
This is very useful when you have a specific page with node_type argument which displays different contents based on the type of a node.

