<?php
/**
 * @file
 * Contains install related hooks.
 */

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 */
function penceo_profile_install() {

  node_access_rebuild(TRUE);

  // Enable theme.
  $themes = array(
    'theme_default' => 'penceo_theme',
    'admin_theme' => 'seven'
  );
  theme_enable($themes);

  foreach ($themes as $var => $theme) {
    if (!is_numeric($var)) {
      variable_set($var, $theme);
    }
  }

  // Set admin theme for node add pages etc.
  variable_set('node_admin_theme', 1);

  drupal_flush_all_caches();

}

/**
 * Callback function for hook_install_tasks().
 *
 * This callback is responsible for adding settings to the database
 * then are modules switched off by default.
 */
function penceo_profile_build_static() {
  $static = array('penceo_static_content');
  penceo_profile_feature_helper($static, TRUE);
}


/**
 * Helper function to check if drush exists.
 *
 * @return NULL if drush exists
 */
function penceo_profile_is_drush() {
  return shell_exec('type drush >/dev/null 2>&1 || { echo 0; }');
}

/**
 * Helper function for reverting features.
 *
 * @param array $features
 *  array of feature names
 *
 * @param bool $disable
 *  indicates if given $features should be disabled after enable/revert
 *
 */
function penceo_profile_feature_helper($features = array(), $disable = FALSE) {
  //check for drush
  $is_drush = is_null(penceo_profile_is_drush());

  //static feature - disable it when settings are already imported
  if ($disable) {
    module_enable($features, TRUE);
    if ($is_drush) {
      //revert features from bash
      foreach ($features as $feature) {
        if (module_exists($feature)) {
          shell_exec("drush fr {$feature} -y --force");
        }
      }
    }
    module_disable($features, TRUE);
  }
  //default feature revert process
  else {
    foreach ($features as $module) {
      if (($feature = feature_load($module, TRUE)) && module_exists($module)) {
        if ($is_drush) {
          shell_exec("drush fr {$module} -y --force");
        }
        else {
          $components = array();

          foreach (array_keys($feature->info['features']) as $component) {
            if (features_hook($component, 'features_revert')) {
              $components[] = $component;
            }
          }

          foreach ($components as $component) {
            features_revert(array($module => array($component)));
          }
        }
      }
    }
  }
  //last clear just in case
  if ($is_drush) {
    shell_exec('drush cc all');
  }
  else {
    drupal_flush_all_caches();
  }
}

/**
 * Callback function for hook_install_tasks().
 *
 * This callback is responsible for reverting all enabled features after
 * site install.
 */
function penceo_profile_revert_features() {

  $features = array(
    //'penceo_wysiwyg',
    'penceo_permission',
  );

  penceo_profile_feature_helper($features);
}

/**
 * Callback function for hook_install_tasks().
 *
 * Assign all available permissions to the administrator role.
 */
function penceo_profile_administrator_permissions() {
  if ($administrator = user_role_load_by_name('administrator')) {
    user_role_grant_permissions($administrator->rid, array_keys(module_invoke_all('permission')));
  }
}

/**
 * Implements hook_install_tasks().
 */
function penceo_profile_install_tasks(&$install_state) {
  $tasks['penceo_profile_build_static'] = array(
    'display_name' => st('Build static content'),
    'display' => TRUE,
    'type' => 'normal',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
  );

  $tasks['penceo_profile_revert_features'] = array(
    'display_name' => st('Revert features'),
    'display' => TRUE,
    'type' => 'normal',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
  );

  $tasks['penceo_profile_administrator_permissions'] = array(
    'display_name' => st('Assign all available permissions to administraor'),
    'display' => TRUE,
    'type' => 'normal',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
  );

  return $tasks;
}
